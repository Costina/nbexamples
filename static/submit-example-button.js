define([
    'jquery', 
    'base/js/namespace',
    'base/js/utils',
    'base/js/i18n',
    'base/js/dialog', 
], function (
    $, 
    Jupyter,
    utils,
    i18n, 
    dialog, 
    ) {
    "use strict";

    function submit_example(event, notebook_name) {
        var settings = {
            processData : false,
            cache : false,
            type : "GET",
            dataType : "json",
            success : submit,
            error : utils.log_ajax_error,
        };
        if(!notebook_name) {
            notebook_name = Jupyter.notebook.notebook_path;
        }
        var url = utils.url_join_encode(
            utils.get_body_data("baseUrl") + 
            '/examples/exist'
        ) + "?filename=" + 
            encodeURIComponent(notebook_name);
        $.ajax(url, settings);
    };
    
    function redirect(data) {
        window.location.reload();
        var url = window.location.protocol +
            "//" +
            window.location.hostname + 
            ":" +
            window.location.port +
            utils.get_body_data("baseUrl") +
            "tree#examples" +
            data;
        var win = window.open(url, Jupyter._target);
        win.focus();
    }
    
    function submit(data) {
        var publish_thunk = function(method) {
            var url = utils.url_join_encode(
                utils.get_body_data("baseUrl") +
                '/examples/' + 
                method
            ) + '?filename=' +
                encodeURIComponent(Jupyter.notebook.notebook_path);
            var settings = {
                processData : false,
                cache : false,
                type : "GET",
                dataType : "json",
                success : redirect,
                error : utils.log_ajax_error,
            };
            $.ajax(url, settings);
        }
        if(data === true) {
            var dialog_body = $('<div/>').append(
                $("<p/>").addClass("rename-message")
                    .text(i18n.msg._('Notebook with that name exists. Enter a new notebook name:'))
                ).append(
                    $("<br/>")
                ).append(
                    $('<input/>').attr('type','text').attr('size','25').addClass('form-control')
                    .val(Jupyter.notebook.notebook_path)
            );
            var d = dialog.modal({
                title: 'Publish',
                keyboard_manager: undefined,
                body: dialog_body,
                buttons: {
                    Rename: {
                        class: "btn-primary",
                        click: function() {
                            var new_name = this.find('input').val();
                            var result = new_name.replace(/\\/g, "/").match(/(.*\/)?(\..*?|.*?)(\.[^.]*?)?(#.*$|\?.*$|$)/);
                            new_name = result[2] + result[3];
                            Jupyter.notebook.rename(new_name);
                            submit_example(null, new_name);
                        }
                    },
                    Overwrite: {
                        class: 'btn-warning',
                        click: function() {
                            $("#submit-example-button").html("Publishing...");
                            return publish_thunk('update');
                        }
                    },
                },
                open: function() {
                    d.find('input').focus();
                }
            });
            $(d).on('hidden.bs.modal', function (e) {
                Jupyter.keyboard_manager.enable();
            });
            $(d).on('shown.bs.modal', function(e) {
                Jupyter.keyboard_manager.disable();
            });
        } else {
            $("#submit-example-button").html("Publishing...");
            return publish_thunk('submit');
        }
    }

    function add_button() {
        if (!Jupyter.toolbar) {
            $([Jupyter.events]).on("app_initialized.NotebookApp", add_button);
            return;
        }

        if ($("#submit-example-button").length === 0) {
            Jupyter.toolbar.add_buttons_group([{
              'label'   : 'Publish',
              'icon'    : 'fa-send',
              'callback': submit_example,
              'id'      : 'submit-example-button'
            }]);
        }
    };

    return {
        load_ipython_extension : add_button
    };
});